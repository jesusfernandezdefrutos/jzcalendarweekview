//
//  JZColumnHeader.swift
//  JZCalendarWeekView
//
//  Created by Jeff Zhang on 28/3/18.
//  Copyright © 2018 Jeff Zhang. All rights reserved.
//

import UIKit

/// Header for each column (section, day) in collectionView (Supplementary View)
open class JZColumnHeader: UICollectionReusableView {
    
    @objc public var lblDay = UILabel()
    @objc public var lblWeekday = UILabel()
    @objc let calendarCurrent = Calendar.current
    @objc let dateFormatter = DateFormatter()
    @objc var stackView = UIStackView()
    
    public override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
        backgroundColor = .clear
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        stackView = UIStackView(arrangedSubviews: [lblDay, lblWeekday])
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        addSubview(stackView)
        lblDay.font = UIFont.systemFont(ofSize: 17)
        lblWeekday.font = UIFont.systemFont(ofSize: 17)
    }
    
    
    @objc public func updateView(date: Date, days: Int, formatoFecha: String) {        
        if days == 1 {
            lblWeekday.text = ""
            dateFormatter.dateFormat = formatoFecha
            lblDay.text = dateFormatter.string(from: date).primeraMayusculaString
            stackView.setAnchorConstraintsEqualTo(centerXAnchor: nil, centerYAnchor: centerYAnchor)
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        } else {
            lblWeekday.text = dateFormatter.shortWeekdaySymbols[calendarCurrent.component(.weekday, from: date) - 1]
            lblDay.text = String(calendarCurrent.component(.day, from: date))
            stackView.setAnchorConstraintsEqualTo(centerXAnchor: centerXAnchor, centerYAnchor: centerYAnchor)
            stackView.superview?.accessibilityIdentifier = "viewIrADia"
            dateFormatter.dateFormat = "dd/MM/yyyy"
            stackView.superview?.accessibilityValue = dateFormatter.string(from: date)
        }
        
        if date.isToday {
            lblDay.textColor = JZWeekViewColors.today
            lblWeekday.textColor = JZWeekViewColors.today
        } else {
            lblDay.textColor = JZWeekViewColors.columnHeaderDay
            lblWeekday.textColor = JZWeekViewColors.columnHeaderDay
        }
        if days == 1 {
            stackView.axis = .horizontal
            lblDay.textAlignment = .left
            lblWeekday.textAlignment = .left
            stackView.alignment = .leading
        } else {
            stackView.axis = .vertical
            lblDay.textAlignment = .left
            lblWeekday.textAlignment = .left
            stackView.alignment = .center
        }
    }
}
